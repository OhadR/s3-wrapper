# AWS S3 Wrapper Library

A handful wrapper to main S3 commands.

this library supports "old" S3-JS-API (`S3 / 'aws-sdk'`) as well as "new" (v3) S3-JS-API (`S3Client, '@aws-sdk/client-s3'`).

**Message from AWS:**

> NOTE: We are formalizing our plans to enter AWS SDK for JavaScript (v2) into maintenance mode in 2023.
>
> Please migrate your code to use AWS SDK for JavaScript (v3).

The creation of S3 client is more flexible because each user-application may has its
own configuration (in previous version, there was only the `region` parameter in the constructor).

usage:

```
const config = {
    credentials: {
      accessKeyId: 'minio-user',
      secretAccessKey: 'minio-password',
    },
    endpoint: 'http://127.0.0.1:9000',
    forcePathStyle: true,
};

const s3api: S3Api = new S3Api(config);
await s3api.uploadFile(bucket, key, file-path);
```

## Build and Deploy

    npm install
    npm run build
    npm version patch
    npm publish
    git push

note that `npm run build` invokes `prebuild` script, to clean dist folder.

## logs

This library uses log4js. However, rather than being dependent on a *specific version* of `log4js`, in order to avoid 
dependencies headache for this library users, it is dependent on `@log4js-node/log4js-api`.

The hosting-application can easily control the log-level of this library. In the unit-tests (`/test/log-config.ts`) there
is a good example:

    categories: {
        default: { appenders: ['out'], level: 'trace' },
        'repos-wrapper': { appenders: ['out'], level: 'debug' }
    }

This way the hosting-application will print its own 'trace' logs (and up), but only 'debug' level logs (and up)
from repos-wrapper.

## Tests

### manual tests

    set AWS_PROFILE=<profile-from-.aws-credentials>
    set AWS_REGION=us-east-1
    ts-node test\internal-tests.ts


### mocha unit-tests

Use the unit tests as example of how to use this library.

load env-vars from file:

    for /F %A in (envs\local-minio.env) do SET %A
    for /F %A in (envs\real-aws.env) do SET %A

run:

    npx mocha --inspect --exit dist\test\main.test.js
    npx mocha --inspect --exit dist\test\**.*.js

## versions

### 1.3.0

* reduce logs: S3 copyObject() from - #4
* check if need to handle ContinuationToken in listObjectsV2 - #2
* listObjects: prefer listObjectsV2 - #1

### 1.4.0

* upload V2: support also readable (not only file) #13
* reduce logs - upload file #12
* tests: upload >1000 objects, to test the listObjects, copyFolder and deleteFolder #11
* exists(): return false only if AWS throws 404 #10
* unit tests: "TooManyBuckets: You have attempted to create more buckets than allowed" #9
* support minio in tests + CI/CD #8
* tests: create bucket on init (and support API) #7
* tests: use logger #6
* add tests for copyObject #5

### 1.4.2 - critical fixes for 1.4.0

* CRITICAL BUG: since adding "test" to tsconfig, path under dist is different so 'customers' cannot import classes #15
* use .npmignore to exclude tests from package #14

### 1.5.0

* support signed url (for V2) #16

### 1.6.0

* support signed url (for V3) #16
* improve tests: use mocha #17
* refactor: make abstract Base class for V2/V3 impl. #18
* only for tests (Minio): cannot create an already-existing-bucket #19

### 1.7.0

* allow extension of APIs (both V2 + V3) #20
* V3 dev: #21, #25, #26, #27, #30, #32
* code reuse among V2 and V3: #22, #29, #33
* tests: #24, #28

### 1.8.0

* upload: allow params `ContentEncoding` and `ContentType` #34
* deprecate getObjectAsText: use AWS' SdkStreamMixin.transformToString() #35
* more units tests - use them as example of how to use this library.

### 1.8.2

* enable upload zipped file with the zip flags #37