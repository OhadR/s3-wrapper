import { S3Base } from "../src/s3/s3-base";
import * as AWS from "aws-sdk";
import * as path from "path";
import { ClientConfiguration } from "aws-sdk/clients/s3";


export const TESTS_BUCKET_NAME=`repos-wrapper-test`;
export const TESTS_DEST_BUCKET_NAME=`repos-wrapper-test-dest`;
export const PATH_TO_TEST='tests';
export const FILE_NAME_IN_S3_NO_EXT = 'test-file';
export const NUM_FILES_TO_UPLOAD = 2500;


//minio config
export function getS3Config() {
    const { S3_ENDPOINT, MINIO_SECRET_KEY, MINIO_ACCESS_KEY } = process.env
    let config : ClientConfiguration = {
        ...(MINIO_ACCESS_KEY && {accessKeyId: MINIO_ACCESS_KEY}) ,
        ...(MINIO_SECRET_KEY && {secretAccessKey: MINIO_SECRET_KEY}),
        ...(S3_ENDPOINT && {endpoint: S3_ENDPOINT}),
        s3ForcePathStyle: true,
        signatureVersion: 'v4'
    }
    console.log(config);
    return config;
}


export async function listFilesInFolder(s3Api: S3Base, bucketName: string, folderName: string) : Promise<number> {

    const objects : AWS.S3.ObjectList = await s3Api.listObjects(bucketName, folderName);
    // logger.info('objects: ');
    // logger.info(objects);
    return objects.length;
}

export async function uploadDataToSourceBucket(s3Api, fileName: string, numFilesToUpload: number) {

    // upload file to bucket several times (to test handling more than 1000 objects)
    const fullFileName = path.join(process.cwd(), 'test', 'resources', fileName);
    const promises = [];
    for(let i = 0; i < numFilesToUpload; ++i) {
        const promise = s3Api.uploadFile(
            TESTS_BUCKET_NAME,
            PATH_TO_TEST + `/${FILE_NAME_IN_S3_NO_EXT}-${i}.txt`,
            fullFileName);
        promises.push(promise);
    }
    await Promise.all(promises);
}

export function generateHugeString() {
    let str = '';
    for(let i = 0; i < 200000; ++i) {
        str += `\n${i}`;
        for(let j = 0; j < 100; ++j)
            str += ' ohads 1234567890';
    }
    return str;
}
