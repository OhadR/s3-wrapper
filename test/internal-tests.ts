import { S3Api } from "../src/s3/s3_api";
import { getLogger } from "log4js";
import { getS3Config, PATH_TO_TEST, TESTS_BUCKET_NAME, TESTS_DEST_BUCKET_NAME } from "./commons";
require('./log-config');
const expect = require('chai').expect;

const logger = getLogger('s3-tests');
logger.level = 'debug';


class S3Tests {

    private s3Api = new S3Api(getS3Config());


    public async run() {
        const is = await this.s3Api.bucketExists(TESTS_BUCKET_NAME);
        logger.info(is);
        await this.s3Api.createBucket(TESTS_BUCKET_NAME);
        await this.s3Api.listObjects(TESTS_BUCKET_NAME, PATH_TO_TEST);
    }
}

new S3Tests().run();