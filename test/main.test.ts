import { expect } from 'chai'
import { getLogger } from "log4js";
import { S3Api } from "../src";
import {
  FILE_NAME_IN_S3_NO_EXT, generateHugeString,
  getS3Config,
  listFilesInFolder, NUM_FILES_TO_UPLOAD,
  PATH_TO_TEST,
  TESTS_BUCKET_NAME,
  TESTS_DEST_BUCKET_NAME,
  uploadDataToSourceBucket
} from "./commons";
import { Readable } from "stream";
import { pipeline } from "stream/promises";
import { GetObjectOutput } from "aws-sdk/clients/s3";
import * as path from "path";
import * as zlib from "zlib";
import { promisify } from "util";
import { createGzip } from "zlib";
import { createReadStream, createWriteStream } from "fs";
require('./log-config');
const logger = getLogger('s3-main-tests');
logger.level = 'debug';

const fileName =
    'test-file.txt';
// 'me_amqa_prod_DryRun.csv';

describe('Test API', function () {
  let s3Api = new S3Api(getS3Config());
  this.beforeAll(async () => {
    if(!await s3Api.bucketExists(TESTS_BUCKET_NAME))
      await s3Api.createBucket(TESTS_BUCKET_NAME);

    if(!await s3Api.bucketExists(TESTS_DEST_BUCKET_NAME))
      await s3Api.createBucket(TESTS_DEST_BUCKET_NAME);

    //clean buckets (in case they already exist):
    let deleted = await s3Api.deleteFolder(TESTS_BUCKET_NAME, PATH_TO_TEST);
    expect(deleted).to.be.true;

    deleted = await s3Api.deleteFolder(TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
    expect(deleted).to.be.true;
  })

  this.afterAll(async () => {
  })



  it('main.test.ts: validate bucket is empty', async () => {
    //validate bucket is empty:
    const items = await listFilesInFolder(s3Api, TESTS_BUCKET_NAME, PATH_TO_TEST);
    expect(items).to.equal(0);
  });

  it('main.test.ts: upload many objects', async () => {

    // test uploadFile(): (upload many objects)
    await uploadDataToSourceBucket(s3Api, fileName, NUM_FILES_TO_UPLOAD);

    // test exists():
    const isExists = await s3Api.exists(TESTS_BUCKET_NAME, `${PATH_TO_TEST}/${FILE_NAME_IN_S3_NO_EXT}-2000.txt`);
    logger.info('assetExistsInBucket? ' + isExists);
    expect(isExists).to.be.true;


    // test listObjects():
    const items = await listFilesInFolder(s3Api, TESTS_BUCKET_NAME, PATH_TO_TEST);
    expect(items).to.equal(NUM_FILES_TO_UPLOAD);
  });

  it('main.test.ts: copy folder', async () => {

    logger.info('= copyFolderTest() =');

    const numFilesInSourcePath = await listFilesInFolder(s3Api, TESTS_BUCKET_NAME, PATH_TO_TEST);

    //first, clean target folder
    let deleted = await s3Api.deleteFolder(TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
    expect(deleted).to.be.true;

    const objects = await s3Api.copyFolder(TESTS_BUCKET_NAME, PATH_TO_TEST, TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);

    //validate all items copied:
    const numFilesInDestPath = await listFilesInFolder(s3Api, TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
    expect(numFilesInDestPath).to.equal(numFilesInSourcePath);
    logger.info('= copyFolderTest() finished =');
  });

  it('main.test.ts: delete folder', async () => {
    logger.info('= deleteFolderTest() =');
    // delete DEST folder:
    await s3Api.deleteFolder(TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);

    const numFilesInDestPath = await listFilesInFolder(s3Api, TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
    expect(numFilesInDestPath).to.equal(0);
  });

  it('main.test.ts: delete object', async () => {
    // test delete(): delete file from SOURCE folder:
    await s3Api.deleteObject(TESTS_BUCKET_NAME, `${PATH_TO_TEST}/${FILE_NAME_IN_S3_NO_EXT}-2000.txt`);

    // test exists():
    const isExists = await s3Api.exists(TESTS_BUCKET_NAME, `${PATH_TO_TEST}/${FILE_NAME_IN_S3_NO_EXT}-2000.txt`);
    logger.info('assetExistsInBucket? ' + isExists);
    expect(isExists).to.be.false;

  });

  it('main.test.ts: test uploadReadable', async () => {
    logger.info('uploadReadable test()');

    const stringToUpload = 'ohad is the man 3!';

    const s = Readable.from(stringToUpload);

    await s3Api.uploadReadable(
        TESTS_BUCKET_NAME,
        PATH_TO_TEST + `/ohad.txt`,
        s);
    //read item:
    const data : GetObjectOutput = await s3Api.getObject(TESTS_BUCKET_NAME, PATH_TO_TEST + `/ohad.txt`);
    const dataString = data.Body.toString();
    expect(dataString).to.equal(stringToUpload);
  });

  it('main.test.ts: test uploadFile', async () => {

    const fullFileName = path.join(process.cwd(), 'test', 'resources', 'test-file.txt');
    await s3Api.uploadFile(
        TESTS_BUCKET_NAME,
        PATH_TO_TEST + `/test-file.txt`,
        fullFileName);
    const exists = await s3Api.exists(TESTS_BUCKET_NAME, PATH_TO_TEST + `/test-file.txt`);
    expect(exists).to.be.true;

    const data: GetObjectOutput = await s3Api.getObject(TESTS_BUCKET_NAME, PATH_TO_TEST + `/test-file.txt`);
    // logger.info("getObject Success", data);
    const dataString = data.Body.toString();
    expect(dataString).to.equal('this is a text file for tests.');
  });

  it('main.test.ts: test uploadFile as zipped', async () => {

    const fullFileName = path.join(process.cwd(), 'test', 'resources', 'test-file.txt');
    const zippedOutput = path.join(process.cwd(), 'dist', 'input.txt.gz');
    const zippenObjectName = `${PATH_TO_TEST}/zip-test-file-${Date.now()}.gz`;

    async function do_gzip(input, output) {
      const gzip = createGzip();
      const source = createReadStream(input);
      const destination = createWriteStream(output);
      await pipeline(source, gzip, destination);
    }

    await do_gzip(fullFileName, zippedOutput)
        .catch((err) => {
          console.error('An error occurred:', err);
          process.exitCode = 1;
        });

    await s3Api.uploadFile(
        TESTS_BUCKET_NAME,
        zippenObjectName,
        zippedOutput,
        {
                  ContentEncoding: 'gzip',
                  ContentType: 'text/html',
                });
    const exists = await s3Api.exists(TESTS_BUCKET_NAME, zippenObjectName);
    expect(exists).to.be.true;

    const dataRaw: GetObjectOutput = await s3Api.getObject(TESTS_BUCKET_NAME, zippenObjectName);
    const dataString = zlib.unzipSync(dataRaw.Body as Buffer);
    logger.info(`dataString: ${dataString}`);
    expect(dataString.toString()).to.equal('this is a text file for tests.');
  });

  it('main.test.ts: test upload zipped Readable', async () => {
    logger.info('upload zipped Readable test()');

    const stringRaw = generateHugeString();
    logger.info(`test-string length: ${stringRaw.length / 1000} Kb`);
    expect(stringRaw.length).to.be.above(300 * 1000 * 1000);  //huge, more than 300M

    const zippedData = zlib.gzipSync(stringRaw);
    const objectName = `${PATH_TO_TEST}/zip-test-${Date.now()}.gz`;

    await s3Api.uploadReadable(
        TESTS_BUCKET_NAME,
        objectName,
        Readable.from(zippedData),
        {
          ContentEncoding: 'gzip',
          ContentType: 'text/html',
        });
    //read item:
    const dataRaw = await s3Api.getObject(TESTS_BUCKET_NAME, objectName);
    const s = zlib.unzipSync(dataRaw.Body as Buffer);
    //validate file size
    expect(stringRaw.length).to.equal(s.length);
  });


});