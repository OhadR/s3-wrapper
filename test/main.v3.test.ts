import { expect } from 'chai'
import { getLogger } from "log4js";
import { S3ApiV3 } from "../src";
import {
  FILE_NAME_IN_S3_NO_EXT, generateHugeString,
  listFilesInFolder, NUM_FILES_TO_UPLOAD,
  PATH_TO_TEST,
  TESTS_BUCKET_NAME,
  TESTS_DEST_BUCKET_NAME,
  uploadDataToSourceBucket
} from "./commons";
import { GetObjectCommandOutput, S3ClientConfig } from "@aws-sdk/client-s3";
import * as path from "path";
import * as zlib from "zlib";
import { Readable } from "stream";
require('./log-config');
const logger = getLogger('s3-v3-main-tests');
logger.level = 'debug';

const fileName =
    'test-file.txt';
// 'me_amqa_prod_DryRun.csv';

//minio config
export function getS3V3Config() {
  const { S3_ENDPOINT, MINIO_SECRET_KEY, MINIO_ACCESS_KEY, AWS_REGION } = process.env
  let config : S3ClientConfig = {
    ...(AWS_REGION && {region: AWS_REGION}),
    ...(S3_ENDPOINT && {endpoint: S3_ENDPOINT}),
    /**
     * Whether to force path style URLs for S3 objects
     * (e.g., https://s3.amazonaws.com/<bucketName>/<key> instead of https://<bucketName>.s3.amazonaws.com/<key>
     */
    forcePathStyle: true,
  }
  if(MINIO_ACCESS_KEY && MINIO_SECRET_KEY) {
    config.credentials = {
      accessKeyId: MINIO_ACCESS_KEY,
      secretAccessKey: MINIO_SECRET_KEY
    }
  }
  console.log(config);
  return config;
}


describe('Test V3 API', function () {
  let s3Api = new S3ApiV3(getS3V3Config());
  this.beforeAll(async () => {
    //TODO implement
    // if(!await s3Api.bucketExists(TESTS_BUCKET_NAME))
    //   await s3Api.createBucket(TESTS_BUCKET_NAME);
    //
    // if(!await s3Api.bucketExists(TESTS_DEST_BUCKET_NAME))
    //   await s3Api.createBucket(TESTS_DEST_BUCKET_NAME);

    //clean buckets (in case they already exist):
    let deleted = await s3Api.deleteFolder(TESTS_BUCKET_NAME, PATH_TO_TEST);
    expect(deleted).to.be.true;

    deleted = await s3Api.deleteFolder(TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
    expect(deleted).to.be.true;
  })

  this.afterAll(async () => {
  })

  it('main.v3.test.ts: validate bucket is empty', async () => {
    //validate bucket is empty:
    const items = await listFilesInFolder(s3Api, TESTS_BUCKET_NAME, PATH_TO_TEST);
    expect(items).to.equal(0);
  });

  it('main.v3.test.ts: upload many objects', async () => {

    // test uploadFile(): (upload many objects)
    await uploadDataToSourceBucket(s3Api, fileName, NUM_FILES_TO_UPLOAD);

    // test exists():
    const isExists = await s3Api.exists(TESTS_BUCKET_NAME, `${PATH_TO_TEST}/${FILE_NAME_IN_S3_NO_EXT}-2000.txt`);
    logger.info('assetExistsInBucket? ' + isExists);
    expect(isExists).to.be.true;


    // test listObjects():
    const items = await listFilesInFolder(s3Api, TESTS_BUCKET_NAME, PATH_TO_TEST);
    expect(items).to.equal(NUM_FILES_TO_UPLOAD);
  });

  //TODO: implement copyObject for V3:
  // it('main.v3.test.ts: copy folder', async () => {
  //
  //   logger.info('= copyFolderTest() =');
  //
  //   const numFilesInSourcePath = await listFilesInFolder(s3Api, TESTS_BUCKET_NAME, PATH_TO_TEST);
  //
  //   //first, clean target folder
  //   let deleted = await s3Api.deleteFolder(TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
  //   expect(deleted).to.be.true;
  //
  //   const objects = await s3Api.copyFolder(TESTS_BUCKET_NAME, PATH_TO_TEST, TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
  //
  //   //validate all items copied:
  //   const numFilesInDestPath = await listFilesInFolder(s3Api, TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
  //   expect(numFilesInDestPath).to.equal(numFilesInSourcePath);
  //   logger.info('= copyFolderTest() finished =');
  // });

  it('main.v3.test.ts: delete folder', async () => {
    logger.info('= deleteFolderTest() =');
    // delete DEST folder:
    await s3Api.deleteFolder(TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);

    const numFilesInDestPath = await listFilesInFolder(s3Api, TESTS_DEST_BUCKET_NAME, PATH_TO_TEST);
    expect(numFilesInDestPath).to.equal(0);
  });

  it('main.v3.test.ts: delete object', async () => {
    // test delete(): delete file from SOURCE folder:
    await s3Api.deleteObject(TESTS_BUCKET_NAME, `${PATH_TO_TEST}/${FILE_NAME_IN_S3_NO_EXT}-2000.txt`);

    // test exists():
    const isExists = await s3Api.exists(TESTS_BUCKET_NAME, `${PATH_TO_TEST}/${FILE_NAME_IN_S3_NO_EXT}-2000.txt`);
    logger.info('assetExistsInBucket? ' + isExists);
    expect(isExists).to.be.false;

  });

  it('main.v3.test.ts: test uploadReadable', async () => {
    logger.info('uploadReadable test()');

    const stringToUpload = 'ohad is the man 3!';
    const objectName = `${PATH_TO_TEST}/upload-readable-test-${Date.now()}.txt`;

    await s3Api.uploadReadable(
        TESTS_BUCKET_NAME,
        objectName,
        Readable.from(stringToUpload),
        {
          ContentLength: stringToUpload.length
        });
    //read item:
    const dataRaw = await s3Api.getObject(TESTS_BUCKET_NAME, objectName);
    const dataString = await dataRaw.Body.transformToString("utf-8");
    expect(dataString).to.equal(stringToUpload);
  });

  it('main.v3.test.ts: test uploadFile', async () => {

    const fullFileName = path.join(process.cwd(), 'test', 'resources', 'test-file.txt');
    const objectName = `${PATH_TO_TEST}/upload-file-test-${Date.now()}.txt`;
    await s3Api.uploadFile(
        TESTS_BUCKET_NAME,
        objectName,
        fullFileName);
    const exists = await s3Api.exists(TESTS_BUCKET_NAME, objectName);
    expect(exists).to.be.true;

    const dataRaw = await s3Api.getObject(TESTS_BUCKET_NAME, objectName);
    const dataString = await dataRaw.Body.transformToString("utf-8");
    expect(dataString).to.equal('this is a text file for tests.');
  });

  it('main.v3.test.ts: test upload zipped Readable', async () => {
    logger.info('upload zipped Readable test()');

    const stringRaw = generateHugeString();
    logger.info(`test-string length: ${stringRaw.length / 1000} Kb`);
    expect(stringRaw.length).to.be.above(300 * 1000 * 1000);  //huge, more than 300M

    const zippedData = zlib.gzipSync(stringRaw);
    logger.info(`zipped-string length: ${zippedData.length / 1000} Kb`);
    const objectName = `${PATH_TO_TEST}/zip-test-${Date.now()}.gz`;

    await s3Api.uploadReadable(
        TESTS_BUCKET_NAME,
        objectName,
        Readable.from(zippedData),
        {
          ContentEncoding: 'gzip',          //MUST
          ContentType: 'text/plain',        //not must, but lets the browser download file as text
          ContentLength: zippedData.length  //MUST, o/w we get error NotImplemented: A header you provided implies functionality that is not implemented
        });
    //read item:
    const dataRaw = await s3Api.getObject(TESTS_BUCKET_NAME, objectName);
    const dataString = await dataRaw.Body.transformToByteArray();
    logger.info(`get obj as ByteArray size = ${dataString.length/1000}`);
    expect(dataString.length).to.equal(zippedData.length);
    const s = zlib.unzipSync(dataString);
    //validate file size
    logger.info(s);
    expect(stringRaw.length).to.equal(s.length);

  });
});