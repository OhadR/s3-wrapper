import {
    S3Client,
    GetObjectCommand,
    DeleteObjectCommand,
    DeleteObjectsCommand,
    HeadObjectCommand,
    ListObjectsV2Command, ListObjectsV2CommandInput, ListObjectsV2Output,
    PutObjectCommand, PutObjectCommandInput, S3ClientConfig, GetObjectCommandOutput,
} from "@aws-sdk/client-s3";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";
import fs = require('fs');
import { Readable } from "stream";
import { S3Base } from "./s3-base";
import { CopyObjectOutput, ObjectIdentifierList, ObjectList } from "aws-sdk/clients/s3";
import { DeleteObjectsCommandInput } from "@aws-sdk/client-s3/dist-types/commands/DeleteObjectsCommand";
const logger = require('@log4js-node/log4js-api').getLogger();
logger.category = 'repos-wrapper-v3';


// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-s3/index.html
// https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/s3-example-creating-buckets.html


export class S3ApiV3 extends S3Base {

    protected s3: S3Client;

    constructor(config: S3ClientConfig) {
        super();
        // Create S3 service object
        this.s3 = new S3Client( config );
    }

    public async listObjects(bucketName: string,
                             folderName: string): Promise<ObjectList> {

        const input: ListObjectsV2CommandInput = {
            Bucket: bucketName,
            Prefix: folderName
        };

        let objects: ObjectList = [];
        let isTruncated;
        do {
            // Call S3 to obtain a list of the objects in the bucket
            const command = new ListObjectsV2Command(input);
            const listOutput : ListObjectsV2Output =  await this.s3.send(command);
            // logger.debug(listOutput);
            logger.trace(`listObjects: ${listOutput.Contents?.length} items.`);

            if(listOutput.Contents)
                objects = objects.concat(listOutput.Contents);

            isTruncated = listOutput.IsTruncated;
            input.ContinuationToken = listOutput.NextContinuationToken;

        } while (isTruncated);
        logger.trace(`listObjects: found total ${objects.length} items.`);
        return objects;
    }

    /**
     * get the object from S3. Note: if object is big, you will wait long time. Use headObject() instead.
     * @param bucketName
     * @param objectName e.g. '20/10/01/7/7vapr427fe9y29f6vz85p0txzp.ast/ohio_merged_lidar.las'
     */
    public async getObject(bucketName: string,
                           objectName: string): Promise<GetObjectCommandOutput> {
        // Create the parameters for calling getObject
        const input = {
            Bucket: bucketName,
            Key: objectName
        };

        const command = new GetObjectCommand(input);

        // Call S3 to fetch an object from the bucket
        return await this.s3.send(command);
    }

    public async deleteObject(bucketName: string,
                              objectName: string): Promise<any> {

        logger.info('S3 deleteObject() ', objectName);

        const input = {
            Bucket: bucketName,
            Key: objectName
        };

        const command = new DeleteObjectCommand(input);

        try {
            return await this.s3.send(command);
        } catch(err) {
            logger.error(err);
            throw err;
        }
    }

    public async deleteObjects(bucketName: string,
                               objectNames: string[]): Promise<any> {

        logger.trace(`deleteObjects(): deleting ${objectNames.length} files from s3 bucket ${bucketName}`);

        let i = 0;
        const promises = [];
        while(i < objectNames.length) {
            const keysListPart = objectNames.slice(i, i + 1000);
            const ObjectDelete :  ObjectIdentifierList = keysListPart.map(key => { return { Key: key }});

            const input : DeleteObjectsCommandInput = {
                Bucket: bucketName,
                Delete: { Objects: ObjectDelete }
            };

            const command = new DeleteObjectsCommand(input);

            const promise = this.s3.send(command);

            promises.push(promise);
            i += 1000;
        }

        await Promise.all(promises);
        logger.trace(`deleteObjects(): objects are deleted from s3 bucket ${bucketName}`);
    }

    public async uploadFile(bucketName: string,
                            objectName: string,
                            fileName: string) {
        // Get file content
        // logger.trace('uploadFile(): bucketName:' + bucketName + ' objectName:' + objectName + ' fileName:' + fileName);
        const fileStream = fs.createReadStream(fileName);
        return await this.uploadReadable(bucketName, objectName, fileStream);
    }

    public async uploadReadable(
        bucketName: string,
        objectName: string,
        readable: Readable,
        moreParams: object = {},
    ) {
        // Get file content
        logger.trace('uploadReadable(): bucketName:' + bucketName + ' objectName:' + objectName);

        // Create the parameters for calling getObject
        const params: PutObjectCommandInput = {
            Bucket: bucketName,
            Key: objectName,
            Body: readable,
            ...moreParams,
        };

        return await this.s3.send( new PutObjectCommand(params) );
    }

    public async exists(bucketName: string,
                        objectName: string): Promise<boolean> {

        logger.debug('S3 exists() ', objectName);

        const input = {
            Bucket: bucketName,
            Key: objectName
        };

        const command = new HeadObjectCommand(input);

        try {
            // Call S3 to obtain the metadata of an object in the bucket
            const data = await this.s3.send(command);
            logger.debug('headObject response: ', data);
            return true;
        } catch(err) {      //err instanceof AWS.AWSError
            if(err['$metadata'].httpStatusCode === 404)
                return false;
            logger.error(err);
            throw err;      //in case of 400 or 403
        }
    }

    public async copyObject(sourceBucketName: string,
                            sourceObjectKey: string,
                            destBucketName: string,
                            destObjectKey: string): Promise<CopyObjectOutput> {
        throw new Error('TBD - Not Implemented Yet.');
    }

    public async getSignedUrl(
        bucketName: string,
        objectName: string) {
        logger.trace('getSignedUrl(): bucketName:' + bucketName + ' objectName:' + objectName);

        const signedUrlExpireSeconds = 60 * 5;

        const getObjectParams = {
            Bucket: bucketName,
            Key: objectName,
        };
        const command = new GetObjectCommand(getObjectParams);
        const signedUrl = await getSignedUrl(this.s3, command, { expiresIn: signedUrlExpireSeconds });

        logger.trace('s3: getSignedUrl response: ', signedUrl);
        return signedUrl;
    }
}