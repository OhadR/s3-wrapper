import * as AWS from 'aws-sdk'
import fs = require('fs');
import {
    CopyObjectOutput,
    CreateBucketOutput, GetObjectOutput,
    ListObjectsV2Output,
    ObjectIdentifierList,
    ObjectList
} from "aws-sdk/clients/s3";
import { Readable } from "stream";
import { S3Base } from "./s3-base";
const logger = require('@log4js-node/log4js-api').getLogger();
logger.category = 'repos-wrapper';

export class S3Api extends S3Base {

    protected s3: AWS.S3;

    constructor(config) {
        super();
        // Create S3 service object
        this.s3 = new AWS.S3(config);
    }

    public async bucketExists(bucketName: string): Promise<boolean> {
        logger.debug(`check if bucketExists: ${bucketName}...`);
        const bucketParams: AWS.S3.HeadBucketRequest = {
            Bucket: bucketName
        };

        try {
            await this.s3.headBucket(bucketParams).promise();
            return true;
        } catch(err) {      //err instanceof AWS.AWSError
            if(err.statusCode === 404)
                return false;
            logger.error(err);
            throw err;      //in case of ! 404
        }
    }

    public async createBucket(bucketName: string): Promise<void> {
        logger.debug(`creating bucket ${bucketName}...`);
        const bucketParams: AWS.S3.CreateBucketRequest = {
            Bucket: bucketName
        };

        const output : CreateBucketOutput = await this.s3.createBucket(bucketParams).promise();
        logger.trace(output);
        logger.trace(`bucket ${bucketName} created successfully`);
    }

    public async listObjects(bucketName: string,
                             folderName: string): Promise<ObjectList> {

        const bucketParams: AWS.S3.ListObjectsV2Request = {
            // MaxKeys: 50, //for tests
            Bucket: bucketName,
            Prefix: folderName,
            ContinuationToken: null
        };

        let objects: ObjectList = [];
        let isTruncated;
        do {
            // Call S3 to obtain a list of the objects in the bucket
            const listOutput : ListObjectsV2Output = await this.s3.listObjectsV2(bucketParams).promise();
            logger.trace(`listObjects in ${bucketName}/${folderName}: ${listOutput.Contents.length} items.`);
            // logger.debug(listOutput);

            objects = objects.concat(listOutput.Contents);

            isTruncated = listOutput.IsTruncated;
            bucketParams.ContinuationToken = listOutput.NextContinuationToken;

        } while (isTruncated);
        logger.trace(`listObjects in ${bucketName}/${folderName}: found total ${objects.length} items.`);
        return objects;
    }

    /**
     * get the object from S3. Note: if object is big, you will wait long time. Use headObject() instead.
     * @param bucketName
     * @param objectName e.g. '20/10/01/7/7vapr427fe9y29f6vz85p0txzp.ast/ohio_merged_lidar.las'
     */
    public async getObject(bucketName: string,
                             objectName: string): Promise<GetObjectOutput> {
        // Create the parameters for calling getObject
        const bucketParams = {
            Bucket: bucketName,
            Key: objectName
        };

        // Call S3 to fetch an object from the bucket
        return await this.s3.getObject(bucketParams).promise();
    }

    public async deleteObject(bucketName: string, objectName: string): Promise<any> {

        logger.info(`S3 deleteObject() bucket=${bucketName}, key=${objectName}`);

        const bucketParams = {
            Bucket: bucketName,
            Key: objectName
        };

        try {
            return await this.s3.deleteObject(bucketParams).promise();
        } catch(err) {
            logger.error(err);
            throw err;
        }
    }

    /**
     * NOTE: s3.deleteObjects handles up to 1000 items. THIS methods gets as many objects and handles the chunks.
     * @param bucketName
     * @param objectNames - Key's of objects to delete
     */
    public async deleteObjects(bucketName: string,
                              objectNames: string[]): Promise<any> {

        logger.trace(`deleteObjects(): deleting ${objectNames.length} files from s3 bucket ${bucketName}`);
        let i = 0;
        const promises = [];
        while(i < objectNames.length) {
            const keysListPart = objectNames.slice(i, i + 1000);
            const ObjectDelete :  ObjectIdentifierList = keysListPart.map(key => { return { Key: key }});
            const promise: Promise<any> = this.s3.deleteObjects({ Bucket: bucketName, Delete: { Objects: ObjectDelete }}).promise();
            promises.push(promise);
            i += 1000;
        }

        let response = await Promise.all(promises);
        logger.trace(`deleteObjects(): objects are deleted from s3 bucket ${bucketName}`);
    }

    public async uploadFile(bucketName: string,
                            objectName: string,
                            fileName: string,
                            moreParams: object = {},) {
        // Get file content
        // logger.trace('uploadFile(): bucketName:' + bucketName + ' objectName:' + objectName + ' fileName:' + fileName);
        const fileStream = fs.createReadStream(fileName);
        return await this.uploadReadable(bucketName, objectName, fileStream, moreParams);
    }

    public async uploadReadable(
        bucketName: string,
        objectName: string,
        fileStream: Readable,
        moreParams: object = {},
    ) {
        // Get file content
        logger.trace(`uploadReadable(): bucketName: ${bucketName} objectName: ${objectName}`);

        fileStream.on('error', function(err) {
            logger.error('File Error', err);
        });

        const params = {
            Bucket: bucketName,
            Key: objectName,
            Body: fileStream,
            ...moreParams,
        };

        // Call S3 to fetch an object from the bucket
        const data = await this.s3.upload(params).promise();
        // logger.trace('s3: uploadFile response: ', data);
        return data;
    }

    public async exists(bucketName: string,
                        objectName: string): Promise<boolean> {

        logger.debug(`S3 exists(): bucketName: ${bucketName} objectName: ${objectName}`);

        const bucketParams = {
            Bucket: bucketName,
            Key: objectName
        };

        try {
            // Call S3 to obtain the metadata of an object in the bucket
            const data = await this.s3.headObject(bucketParams).promise();
            // logger.trace('headObject response: ', data);
            return true;
        } catch(err) {      //err instanceof AWS.AWSError
            if(err.statusCode === 404)
                return false;
            logger.error(err);
            throw err;      //in case of 400 or 403
        }
    }

    public async copyObject(sourceBucketName: string,
                            sourceObjectKey: string,
                            destBucketName: string,
                            destObjectKey: string): Promise<CopyObjectOutput> {

        logger.trace(`S3 copyObject() from ${sourceBucketName}/${sourceObjectKey} to ${destBucketName}/${destObjectKey} `);

        const copyParams = {
            CopySource: `${sourceBucketName}/${sourceObjectKey}`,
            Bucket: destBucketName,
            Key: destObjectKey
        };

        try {
            return await this.s3.copyObject(copyParams).promise();
        } catch(err) {
            logger.error(`error copying srcPath: ${sourceBucketName}/${sourceObjectKey}, dstPath: ${destBucketName}/${destObjectKey}, err: ${err}`);
            throw err;
        }
    }

    public async getSignedUrl(
        bucketName: string,
        objectName: string) {
        logger.trace('getSignedUrl(): bucketName:' + bucketName + ' objectName:' + objectName);

        const signedUrlExpireSeconds = 60 * 5;

        const bucketParams = {
            Bucket: bucketName,
            Key: objectName,
            Expires: signedUrlExpireSeconds
        };

        const signedUrl = await this.s3.getSignedUrlPromise('getObject', bucketParams);
        logger.trace('s3: getSignedUrl response: ', signedUrl);
        return signedUrl;
    }

}