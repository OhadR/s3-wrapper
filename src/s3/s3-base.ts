import { Readable } from "stream";
import { CopyObjectOutput, ObjectList } from "aws-sdk/clients/s3";
import * as AWS from "aws-sdk";

const logger = require('@log4js-node/log4js-api').getLogger();
logger.category = 'repos-wrapper-base';

export abstract class S3Base {

    abstract getObject(bucketName: string, objectName: string);

    /**
     * S3 SDK lists up to MaxKeys (default and max to 1000). this wrapper handles even more than 1000 items in a single call.
     * @param bucketName
     * @param folderName
     */
    abstract listObjects(bucketName: string, folderName: string): Promise<ObjectList>;
    abstract deleteObject(bucketName: string, objectName: string): Promise<any>;
    abstract deleteObjects(bucketName: string, objectNames: string[]): Promise<any>;
    abstract copyObject(sourceBucketName: string,
                            sourceObjectKey: string,
                            destBucketName: string,
                            destObjectKey: string): Promise<CopyObjectOutput>;

/*
    public async getObjectAsText(bucketName: string, objectName: string): Promise<string> {

        const data = await this.getObject(bucketName, objectName);

        const bodyContents = await this.streamToString(data.Body);
        logger.debug(bodyContents);
        return bodyContents;
    }

    // Create a helper function to convert a ReadableStream to a string.
    async streamToString(stream : Readable): Promise<string> {
        return new Promise((resolve, reject) => {
            const chunks = [];
            stream.on("data", (chunk) => chunks.push(chunk));
            stream.on("error", reject);
            stream.on("end", () => resolve(Buffer.concat(chunks).toString("utf8")));
        });
    }
*/

    /**
     * recursively empties S3 folder and deletes it.
     * @param bucketName
     * @param folder
     */
    public async deleteFolder(bucketName: string,
                              folder: string): Promise<boolean> {

        logger.debug(`deleteFolder( ${bucketName}/${folder} )`);
        //first make sure folder is empty, otherwise it will not be deleted:
        await this.emptyS3Directory(bucketName, folder);

        try {
            // after 'folder' is empty, delete the folder itself:
            const data = await this.deleteObject(bucketName, folder);
            return true;
        } catch(err) {
            logger.error(err);
            return false;
        }
    }

    //credit: https://stackoverflow.com/a/48955582/421642
    protected async emptyS3Directory(bucket: string, dir: string) {

        const listedObjects = await this.listObjects(bucket, dir);

        logger.debug(`emptyS3Directory(): deleting ${listedObjects.length} items recursively from ${bucket}/${dir}`);

        //if no contents found, do nothing
        if (!listedObjects)
            return;

        await this.deleteObjects(bucket, listedObjects.map(item => item.Key));
        logger.debug(`emptyS3Directory(): all files are deleted from s3 path ${bucket}/${dir}`);
    }

    /**
     * copies a folder A from a bucket, to folder A in another(!) bucket
     * @param sourceBucketName
     * @param sourcePath
     * @param destinationBucket
     * @param destinationPath
     */
    public async copyFolder(sourceBucketName: string,
                            sourcePath: string,
                            destinationBucket: string,
                            destinationPath: string): Promise<void> {
        const listedObjects : AWS.S3.ObjectList = await this.listObjects(sourceBucketName, sourcePath);
        logger.debug(`copyFolder: listedObjects in ${sourceBucketName}/${sourcePath}: ${listedObjects.length} items.`);

        if (listedObjects.length === 0)
            return;

        const promises: Promise<AWS.S3.Types.CopyObjectOutput>[] = [];
        for(let i = 0; i < listedObjects.length; ++i) {

            const s3object = listedObjects[i];
            let newKey = s3object.Key.replace(sourcePath, destinationPath);

            // Call S3 to obtain a list of the objects in the bucket
            const promise: Promise<CopyObjectOutput> = this.copyObject(
                sourceBucketName, s3object.Key,
                destinationBucket, newKey);
            promises.push(promise);
        }
        await Promise.all(promises);
        logger.debug('done copying.');
    }
}